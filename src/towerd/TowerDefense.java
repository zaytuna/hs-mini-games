package towerd;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;

import util.Methods;

public class TowerDefense extends JPanel implements Runnable, MouseListener, MouseMotionListener {
	private static final long serialVersionUID = 1541296238877513615L;

	public static final Dimension SCREEN = Toolkit.getDefaultToolkit().getScreenSize();

	// Essentials
	public GameObject[][] map;
	public int gridWidth, gridHeight;
	public ArrayList<Battery> bats;
	public ArrayList<Projectile> proj;
	public ArrayList<PowerLine> lines;
	public int waiting;
	public long cooldown = 500000000L;

	public double energy = 100, maxEnergy = 100;
	public double happiness = 100, maxHappiness = 200;

	// Level stuff
	Objective objective, nextObj;
	BatteryBoss boss = new BatteryBoss();
	public ArrayList<Point> start;
	int secondsDay = 120, secondsNight = 30;

	int[] cost = { 10, 20, 100, 40, 7, 20, 0 };
	String[] names = { "Destroy", "Tower", "Generator", "E-System", "Path", "Power Lines", "Selection" };

	// GUI stuff
	BeltTile prevDrag = null;
	Generator startPLineDrag = null;
	Point pretend = null;
	int[] buttonPos = { -1, 0 };
	int barspace = 150, selectedMouseFunction = 0;
	double slider = 0;

	public int fps;
	public long prevTime = System.nanoTime(), lastTime = System.nanoTime();

	// vars for keeping track of where I am
	GameMenu menu;
	int gameMode = -1;// gameMode of 0 is running, 1 is
	// paused, -1 is menu, 2 is dead, 3 is tutorial
	int startTileCounter = 0, counter = 0;
	int day = 0;// always counts down, up to -(time in night) then switching to

	// (time in day)

	public TowerDefense() {
		super(true);

		setLayout(null);

		addMouseListener(this);
		addMouseMotionListener(this);

		bats = new ArrayList<Battery>();
		proj = new ArrayList<Projectile>();
		start = new ArrayList<Point>();
		lines = new ArrayList<PowerLine>();
		menu = new GameMenu(this);
		changeGrid(30, 23);
		// changeGrid(10, 10);
		// createPath();

		setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
	}

	public void changeGrid(int x, int y) {
		gridWidth = Math.min((SCREEN.width - barspace) / x, SCREEN.height / y);
		gridHeight = gridWidth;

		map = new GameObject[x][y];
		if (map != null)
			for (int i = 0; i < map.length && i < x; i++)
				for (int j = 0; j < map[i].length && j < y; j++)
					map[i][j] = new BlankTile(i, j);

		start.add(new Point(0, map[0].length / 2));
	}

	public void createPath() {
		for (int i = 0; i < map.length; i++)
			for (int j = 0; j < map[i].length; j++)
				map[i][j] = new BlankTile(i, j);

		BeltTile prev = null;
		for (int i = 0; i < map.length; i++) {
			BeltTile a = new BeltTile(i, map[i].length / 2);
			map[i][map[i].length / 2] = a;
			if (prev != null)
				prev.next.add(new Point(a.x, a.y));

			prev = a;
		}
	}

	public void run() {
		try {
			while (true) {
				update();
				repaint();
				Thread.sleep(10);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void newWave(int num) {
		waiting += num;
	}

	public void update() {
		boss.update();

		if (gameMode == 0) {
			if (happiness <= 0) {
				boss.setText("YOU'RE FIRED!!!!");
				gameMode = 2;
			}

			// if (waiting == 0 && !timer.running())
			// timer.start();

			slider += (selectedMouseFunction - slider) / 4;
			for (GameObject[] q : map)
				for (GameObject r : q)
					if (r != null)
						r.update(this);

			for (int i = 0; i < lines.size(); i++)
				lines.get(i).update(this);

			if (energy < maxEnergy)
				energy += .1;

			for (int i = 0; i < bats.size(); i++) {
				Battery b = bats.get(i);
				b.update(this);
			}

			long temp = System.nanoTime();
			if (waiting > 0 && temp - lastTime >= cooldown) {
				Point init = start.get((startTileCounter++) % start.size());
				bats.add(new Battery(init.x, init.y, 300));
				waiting--;
				lastTime = temp;
			}

			for (int i = 0; i < proj.size(); i++)
				proj.get(i).update(this);
		}
	}

	public static void main(String args[]) {
		JFrame frame = new JFrame();
		TowerDefense td = new TowerDefense();

		frame.add(td, BorderLayout.CENTER);
		frame.setBounds(0, 0, SCREEN.width, SCREEN.height);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		frame.setTitle("Assembly");
		frame.setUndecorated(true);
		frame.setVisible(true);
		Thread d = new Thread(td);
		d.setPriority(Thread.MAX_PRIORITY);
		d.start();
	}

	public void paintComponent(Graphics gr) {
		gr.setColor(new Color(0, 0, 10));
		gr.fillRect(0, 0, SCREEN.width, SCREEN.height);
		if (gameMode == -1) {
			menu.paint(gr);
			boss.paint(gr);
		} else {
			Graphics2D g = (Graphics2D) gr;
			g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

			int oX = (SCREEN.width - gridWidth * map.length + 150) / 2, oY = (SCREEN.height - gridHeight
					* map[0].length) / 2;

			int w = gridWidth, h = gridHeight;
			for (int i = 0; i < map.length; i++) {
				for (int j = 0; j < map[i].length; j++) {
					if (map[i][j] != null)
						g.drawImage(map[i][j].paint(w, h), oX + i * w, oY + j * h, this);
				}
			}

			for (int i = 0; i < bats.size(); i++) {
				Battery b = bats.get(i);
				g.drawImage(b.paint(2 * w / 3, 2 * h / 3), oX + w / 6 + (int) (b.x * w + b.dx), oY + h / 6
						+ (int) (b.y * h + b.dy), this);
			}

			g.setStroke(new BasicStroke(5));

			for (PowerLine pl : lines)
				pl.paint(g, oX + pl.start.x * w + w / 2, oY + pl.start.y * h + h / 2, oX + pl.endX * w + w / 2, oY
						+ pl.endY * h + h / 2);
			g.setColor(Color.PINK);
			if (pretend != null)
				g.drawLine(oX + startPLineDrag.x * w + w / 2, oY + startPLineDrag.y * h + h / 2, oX + pretend.x * w + w
						/ 2, oY + pretend.y * h + h / 2);

			g.setColor(Color.ORANGE);
			for (int i = 0; i < proj.size(); i++) {
				Projectile j = proj.get(i);
				// if (proj.get(i).MOVE_TYPE == 0)
				// g.fillOval((int) j.x, (int) j.y, 10, 10);
				// else if (proj.get(i).MOVE_TYPE == 1) {
				g.drawLine((int) j.x, (int) j.y, (int) (j.x - 2 * j.vx), (int) (j.y - 2 * j.vy));
				// }
			}

			g.setColor(Color.CYAN);
			// draw start positions
			for (Point p : start) {
				double r = Math.abs(Math.sin(counter++ / 60D)) - .5;
				for (int i = 0; i < 5; i++) {
					r *= 1.4;
					g.drawOval(oX + p.x * w - (int) (w * r), oY + p.y * h - (int) (h * r), (int) (w * (2 * r + 1)),
							(int) (h * (2 * r + 1)));
				}
			}

			// draw objective markers
			if (objective != null) {
				g.setColor(Methods.colorMeld(new Color(0, 0, 0, 0), new Color(50, 0, 250), objective.secondsLeft
						/ (double) objective.seconds));
				int cx = oX + objective.x * w + w / 2, cy = oY + objective.y * h + h / 2;
				double theta = counter / 60D, r = 100;
				for (int i = 0; i < 3; i++) {
					theta += 2 * Math.PI / 3;
					g.drawLine(cx, cy, cx + (int) (r * Math.sin(theta)), cy + (int) (r * Math.cos(theta)));
				}
				g.setColor(Color.WHITE);
				g.setFont(new Font("SANS_SERIF", Font.BOLD, 20));
				Methods.drawCenteredText(g, objective.collected + "/" + objective.num, cx, cy, 0, 0);
			}

			if (nextObj != null) {
				g.setColor(Methods.colorMeld(new Color(50, 0, 250), new Color(0, 0, 0, 0), objective.secondsLeft
						/ (double) objective.seconds));
				g.drawRect(oX + nextObj.x * w + w / 3, oY + nextObj.y * h + w / 3, w / 3, h / 3);
			}

			g.setStroke(new BasicStroke(1));

			// *******************************************************************

			// draw energy skeletons
			g.setColor(new Color(50, 50, 50));
			g.fillRect(38, SCREEN.height / 3, 4, SCREEN.height * 2 / 3);
			g.fillRect(78, SCREEN.height / 3, 4, SCREEN.height * 2 / 3);
			g.fillRect(30, SCREEN.height / 3 - 4, 20, 4);
			g.fillRect(70, SCREEN.height / 3 - 4, 20, 4);

			// draw buttons
			g.setFont(new Font("COURIER", Font.PLAIN, 16));
			drawButton(g, 0, new Color(255, 255, 200), 3, 1, 72, 29, "More");
			drawButton(g, 1, new Color(200, 255, 200), 75, 1, 72, 29, "Ready");

			// draw energies
			g.setColor(new Color(255, 255, 50));
			g.fillRect(30, (int) (SCREEN.height - energy * SCREEN.height * 2 / 3 / maxEnergy), 20, (int) (energy
					* SCREEN.height * 2 / 3 / maxEnergy));

			g.setColor(new Color(80, 140, 0));
			g.fillRect(70, (int) (SCREEN.height - happiness * SCREEN.height * 2 / 3 / maxHappiness), 20,
					(int) (happiness * SCREEN.height * 2 / 3 / maxHappiness));

			// draw menu
			for (int i = 0; i < names.length; i++) {
				g.setColor(i == selectedMouseFunction ? Color.MAGENTA : new Color(80, 140, 0));
				g.drawString(names[i], 25, i * 25 + 80);
			}

			g.setColor(Color.MAGENTA);
			g.fillOval(5, (int) (slider * 25 + 71), 10, 10);

			boss.paint(g);

			// fps counter
			g.setColor(Color.WHITE);
			long time = System.nanoTime();
			fps = 1000000000 / (int) (time - prevTime);
			g.drawString("FPS: " + fps, SCREEN.width - 100, SCREEN.height - 30);
			prevTime = time;
		}

	}

	private void drawButton(Graphics g, int i, Color color, int j, int k, int l, int m, String string) {
		if (buttonPos[i] == -1)
			g.setColor(Color.DARK_GRAY);
		else if (buttonPos[i] == 0)
			g.setColor(color);
		else if (buttonPos[i] == 1)
			g.setColor(color.darker());

		g.fillRect(j, k, l, m);
		g.setColor(Color.BLACK);
		Methods.drawCenteredText(g, string, j, k, l, m);
		g.drawRect(j, k, l, m);
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
	}

	@Override
	public void mousePressed(MouseEvent evt) {
		if (gameMode == -1)
			menu.mousePressed(evt);

		else {
			if (evt.getX() < 150 && evt.getY() <= 30) {
				if (evt.getX() < 75 && buttonPos[0] != -1)
					buttonPos[0] = 1;
				else if (buttonPos[1] != -1)
					buttonPos[1] = 1;
			}

			int i = getXFromPix(evt.getX()), j = getYFromPix(evt.getY());
			if (selectedMouseFunction == 4 && i >= 0 && j >= 0 && i < map.length && j < map[i].length
					&& map[i][j] instanceof BlankTile) {
				prevDrag = new BeltTile(i, j);
				map[i][j] = prevDrag;
			}
			if (selectedMouseFunction == 5 && i >= 0 && j >= 0 && i < map.length && j < map[i].length
					&& map[i][j] instanceof Generator) {
				startPLineDrag = (Generator) map[i][j];
			}
		}
	}

	@Override
	public void mouseReleased(MouseEvent evt) {
		if (boss.inPosition) {
			boss.hide();
			if (gameMode == 2)
				gameMode = -1;
		}

		if (gameMode == -1)
			menu.mouseReleased(evt);
		else {
			prevDrag = null;

			if (evt.getX() < 150) {
				for (int i = 0; i < names.length; i++)
					if (evt.getY() < i * 25 + 87 && evt.getY() >= (i - 1) * 25 + 87)
						selectedMouseFunction = i;
				if (evt.getY() <= 30) {
					if (evt.getX() < 75 && buttonPos[0] != -1) {
						buttonPos[0] = 0;
						newWave(15);
					} else if (evt.getX() >= 75 && buttonPos[1] != -1) {
						buttonPos[1] = 0;
						start();
					}
				}
			} else
				compute(evt.getX(), evt.getY(), 0);
		}
	}

	private void start() {
		objective = Objective.createStatic(map.length, map[0].length, 800);
		buttonPos[1] = -1;
		buttonPos[0] = 0;
		nextObj = null;
		map[objective.x][objective.y] = objective;
	}

	public int getXFromPix(int x) {
		int oX = (SCREEN.width - gridWidth * map.length + 150) / 2;
		return (x - oX) / gridWidth;
	}

	int getXDistFromPix(int x) {
		int oX = (SCREEN.width - gridWidth * map.length + 150) / 2;
		return (x - oX) % gridWidth;
	}

	public int getYFromPix(int y) {
		int oY = (SCREEN.height - gridHeight * map[0].length) / 2;
		return (y - oY) / gridHeight;
	}

	int getYDistFromPix(int y) {
		int oY = (SCREEN.height - gridHeight * map[0].length) / 2;
		return (y - oY) % gridHeight;
	}

	private void compute(int x, int y, int type) {
		if (gameMode == 0) {
			int i = getXFromPix(x);
			int j = getYFromPix(y);

			if (i >= 0 && i < map.length && j >= 0 && j < map[i].length)
				switch (selectedMouseFunction) {
				case 0:// Destroy
					if (!(map[i][j] instanceof BlankTile || map[i][j] instanceof Objective)
							&& energy >= getCost(i, j, 0)) {
						energy -= getCost(i, j, 0);
						map[i][j] = new BlankTile(i, j);
					}
					break;
				case 1:// Tower
					if (type == 0 && energy >= getCost(i, j, 1) && map[i][j] instanceof BlankTile) {
						energy -= getCost(i, j, 1);
						map[i][j] = new Tower(i, j);
					}
					break;
				case 2:// Generators
					if (type == 0 && energy >= getCost(i, j, 2) && map[i][j] instanceof BlankTile) {
						energy -= getCost(i, j, 2);
						map[i][j] = new Generator(i, j);
					}
					break;
				case 3:// E-Tower
					break;
				case 4:// Path
					// if (getXDistFromPix(x) < gridHeight / 8 ||
					// getXDistFromPix(x)
					// > 7 * gridHeight / 8
					// || getYDistFromPix(y) < gridWidth / 8 ||
					// getYDistFromPix(y) >
					// 7 * gridWidth / 8)
					// return;
					if (type == 1 && energy >= getCost(i, j, 4)) {
						if (map[i][j] instanceof BlankTile) {
							energy -= getCost(i, j, 4);
							BeltTile tile = new BeltTile(i, j);
							if (prevDrag != null) {
								prevDrag.next.add(new Point(i, j));
								prevDrag.im = null;
								prevDrag = tile;
							}
							map[i][j] = tile;
						} else if (map[i][j] instanceof BeltTile && prevDrag != null
								&& (i != prevDrag.x || j != prevDrag.y)) {
							energy -= getCost(i, j, 4);
							prevDrag.next.add(new Point(i, j));
							prevDrag.im = null;
							prevDrag = (BeltTile) map[i][j];

						} else if (map[i][j] instanceof BeltTile && prevDrag == null) {
							energy -= getCost(i, j, 4);
							prevDrag = (BeltTile) map[i][j];
						} else if (map[i][j] instanceof Objective && prevDrag != null
								&& (i != prevDrag.x || j != prevDrag.y)) {
							energy -= getCost(i, j, 4);
							prevDrag.next.add(new Point(i, j));
							prevDrag.im = null;
							prevDrag = null;
						}
					}
					break;
				case 5:
					if (type == 0 && startPLineDrag != null && energy >= getCost(i, j, 5)) {// release
						// line
						lines.add(new PowerLine(startPLineDrag, new Point(i, j)));
						pretend = null;
						energy -= getCost(i, j, 5);
					} else if (type == 1 && startPLineDrag != null && energy >= getCost(i, j, 5))
						pretend = new Point(i, j);

					break;
				case 6:
					break;
				}
		}
	}

	private int getCost(int i, int j, int type) {
		switch (type) {
		case 0:
			if (map[i][j] instanceof Rubbish)
				return ((Rubbish) map[i][j]).number;
			else if (map[i][j] instanceof Tower)
				return 10;
		case 4:
			return prevDrag == null ? cost[4] : cost[4]
					* ((prevDrag.x - i) * (prevDrag.x - i) + (prevDrag.y - j) * (prevDrag.y - j));
		default:
			return cost[type];
		}
	}

	@Override
	public void mouseDragged(MouseEvent evt) {
		if (evt.getX() > 150)
			compute(evt.getX(), evt.getY(), 1);
	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
	}

	public int pixFromX(double d) {
		int oX = (SCREEN.width - gridWidth * map.length + 150) / 2;
		return (int) (oX + d * gridWidth);
	}

	public int pixFromY(double d) {
		int oY = (SCREEN.height - gridHeight * map[0].length) / 2;
		return (int) (oY + d * gridHeight);
	}

	public void reset() {
		start.clear();
		proj.clear();
		lines.clear();

		changeGrid(30, 23);
		objective = null;
		nextObj = null;

		startTileCounter = 0;
		counter = 0;

		happiness = 100;
		maxHappiness = 200;
		energy = 100;
		maxEnergy = 100;
		waiting = 0;
	}
}