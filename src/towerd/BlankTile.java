package towerd;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;

import util.Methods;

public class BlankTile extends GameObject {
	public BlankTile(int x, int y) {
		super(x, y);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Image paint(int width, int height) {
		if (im == null || im.getWidth() != width || im.getHeight() != height) {
			im = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
			Graphics2D g = im.createGraphics();
			g.setColor(Methods.colorMeld(Color.BLACK, new Color(100, 70, 0), power / 200D));
			g.fillRect(0, 0, width - 1, height - 1);
			g.setColor(new Color(50, 50, 25));
			g.drawRect(0, 0, width - 1, height - 1);
			g.dispose();
		}
		return im;
	}

	@Override
	public void update(TowerDefense td) {
		power *= .999;
	}

}
