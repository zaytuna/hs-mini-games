package towerd;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;

import util.Methods;

public class Tower extends GameObject {
	int damage = 50;
	int reload = 150;
	int range = 4;
	int cost = 50;
	double reloading = 0;
	int effect = 0;// 0 = nothing; 1 == explode, 2 = stick, 3 = ?, 4 =
	// penetration, 5 = bounce
	int movetype = 1;// 1 = go straight; 0 = home in on target; 2 = go to target

	public BufferedImage im;

	public Tower(int x, int y) {
		super(x, y);
	}

	@Override
	public void update(TowerDefense td) {
		if (reloading >= 1)
			for (int i = 0; i < td.bats.size(); i++)
				if (td.bats.get(i).filled < td.bats.get(i).capacity && inRange(td.bats.get(i))) {
					Projectile p = new Projectile(this, td);
					payForShot(td);
					p.start(td.bats.get(i), td);
					reloading = 0;

					if (effect != 4)
						return;
				}

		if (reloading < 1)
			reloading += 1 / (double) (reload / (power / 50 + 1));
	}

	private void payForShot(TowerDefense td) {
		int r = 1;
		for (int i = -r; i < r; i++)
			for (int j = -r; j < r; j++)
				if (i + x >= 0 && i + x < td.map.length && j + y >= 0 && j + y < td.map[i + x].length)
					td.map[i + x][j + y].power = (int) Math.max(0, Math.min(200, td.map[i + x][y + j].power - cost
							/ (1 + Math.sqrt(i * i + j * j))));
	}

	public boolean inRange(Battery b) {
		return (b.x - x) * (b.x - x) + (b.y - y) * (b.y - y) <= range * range;
	}

	@Override
	public Image paint(int width, int height) {
		// if (im == null || im.getWidth() != width || im.getHeight() != height)
		im = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

		Graphics2D g = im.createGraphics();
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		// g.setComposite(AlphaComposite.getInstance(AlphaComposite.CLEAR,
		// 0.0f));
		// g.fillRect(0, 0, width, height);
		g.setColor(Methods.colorMeld(Color.BLACK, new Color(100, 70, 0), power / 200D));
		g.fillRect(0, 0, width, height);
		g.setColor(Methods.colorMeld(Color.GRAY, Color.ORANGE, power / 200D));
		int d = (int) ((width - 10) * (1 - reloading)) / 2;
		g.fillOval(5 + d, 5 + d, width - 10 - 2 * d, height - 10 - 2 * d);

		g.setStroke(new BasicStroke(5));
		// g.setColor(Color.GRAY);
		g.drawOval(5, 5, width - 10, height - 10);

		g.dispose();
		return im;
	}
}
