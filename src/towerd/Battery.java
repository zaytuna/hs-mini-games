package towerd;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.image.BufferedImage;

import util.Methods;

public class Battery extends GameObject {
	public double dx, dy, speed = .5;
	public int capacity, filled;
	private boolean whetherBatteryHasTraveledToTheCenterOfTheBeltTileYet = false;
	Point choice = null;
	private boolean requiresRepaint = false;

	public Battery(int x, int y, int c) {
		super(x, y);
		capacity = c;
		filled = 0;
	}

	public void update(TowerDefense td) {
		if (td.map[x][y] instanceof BeltTile) {
			choice = (choice == null) ? choosePath(((BeltTile) td.map[x][y])) : choice;

			if (choice != null) {
				if (whetherBatteryHasTraveledToTheCenterOfTheBeltTileYet) {
					dx += speed * (choice.x - x);
					dy += speed * (choice.y - y);

				} else {
					double dist = Math.sqrt(dx * dx + dy * dy) + .01;
					dx += speed * -dx / dist;
					dy += speed * -dy / dist;

					if (Math.abs(-dx) < 2 && Math.abs(-dy) < 2)
						whetherBatteryHasTraveledToTheCenterOfTheBeltTileYet = true;
				}

				if (im == null)
					paint(td.gridWidth * 2 / 3, td.gridHeight * 2 / 3);

				if (whetherBatteryHasTraveledToTheCenterOfTheBeltTileYet
						&& (dx > td.gridWidth / 2 || dy > td.gridHeight / 2 || dx <= -td.gridWidth / 2 || dy <= -td.gridHeight / 2)) {

					whetherBatteryHasTraveledToTheCenterOfTheBeltTileYet = false;

					dx = -Math.signum(choice.x - x) * td.gridWidth / 2;
					dy = -Math.signum(choice.y - y) * td.gridHeight / 2;

					x = choice.x;
					y = choice.y;

					choice = null;
				}
			}
		} else {
			if (td.map[x][y] instanceof Objective) {
				((Objective) td.map[x][y]).collect(td, this);
			} else {
				td.happiness -= 10;
				if (td.map[x][y] instanceof Rubbish) {
					((Rubbish) td.map[x][y]).number++;
					((Rubbish) td.map[x][y]).im = null;
				} else
					td.map[x][y] = new Rubbish(x, y);
			}
			td.bats.remove(this);

		}
	}

	private Point choosePath(BeltTile cTile) {
		if (cTile != null)
			return cTile.pollNext();
		return null;
	}

	public Image paint(int width, int height) {
		if (im == null || im.getWidth() != width || im.getHeight() != height || requiresRepaint) {
			if (im == null || im.getWidth() != width || im.getHeight() != height)
				im = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);

			Graphics2D g = im.createGraphics();

			// g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
			// RenderingHints.VALUE_ANTIALIAS_ON);

			g.setColor(Color.BLACK);
			g.fillOval(0, 0, width, height);

			// filled = capacity/2;

			g.setColor(Methods.colorMeld(new Color(255, 0, 0), new Color(0, 175, 0), (double) filled / capacity));
			g.fillOval((width - (int) (width * Math.sqrt(filled / (double) capacity))) / 2 + 1,
					(height - (int) (height * Math.sqrt(filled / (double) capacity))) / 2 + 1, (int) (width
							* Math.sqrt(filled / (double) capacity) - 2), (int) (height
							* Math.sqrt(filled / (double) capacity) - 2));

			g.dispose();
		}

		return im;
	}

	public void fill(int damage) {
		requiresRepaint = true;
		filled = Math.min(capacity, filled + damage);
	}
}