package towerd;

public class Projectile {
	Battery target;
	Tower owner;
	double vx, vy, x, y;
	int speed, MOVE_TYPE;

	public Projectile(Tower owner, TowerDefense td) {
		this.owner = owner;

		x = td.pixFromX(owner.x) + td.gridWidth / 2;
		y = td.pixFromY(owner.y) + td.gridHeight / 2;

		MOVE_TYPE = owner.movetype;
		speed = 4;
	}

	public void update(TowerDefense td) {
		if (x < 0 || y < 0 || x > TowerDefense.SCREEN.width || y > TowerDefense.SCREEN.height)
			td.proj.remove(this);

		switch (MOVE_TYPE) {
		case 0:
			vx = (td.pixFromX(target.x) + target.dx + td.gridWidth / 3 - x) / speed;
			vy = (td.pixFromY(target.y) + target.dy + td.gridHeight / 3 - y) / speed;

			if (contact(td, target)) {
				target.fill(owner.damage);
				td.proj.remove(this);
			}
			break;
		case 1:
			for (int i = 0; i < td.bats.size(); i++) {
				if (contact(td, td.bats.get(i)) && (td.bats.get(i).capacity > td.bats.get(i).filled)) {
					td.bats.get(i).fill(owner.damage);
					if (owner.effect != 3) {
						td.proj.remove(this);
						break;
					}
				}
			}
			break;
		}

		x += vx;
		y += vy;
	}

	// broken TODO:Fixit
	private boolean contact(TowerDefense td, Battery b) {
		return (Math.abs((td.pixFromX(b.x) + b.dx + td.gridWidth / 3 - x)) <= td.gridWidth / 3 && Math.abs(td
				.pixFromY(b.y)
				+ b.dy + td.gridHeight / 3 - y) <= td.gridHeight / 3);
	}

	public void start(Battery b, TowerDefense td) {
		switch (MOVE_TYPE) {
		case 1:
			double d = distance(td, b, x, y);
			vx = (int) (100 * (td.pixFromX(b.x) + b.dx - x + td.gridWidth / 2) / (d + 100));
			vy = (int) (100 * (td.pixFromY(b.y) + b.dy - y + td.gridHeight / 2) / (d + 100));
			break;
		case 0:
			target = b;
		}
		td.proj.add(this);
	}

	private int distance(TowerDefense td, Battery b, double x, double yb) {
		return (int) (Math.sqrt(Math.pow(td.pixFromX(b.x) + b.dx, 2) + Math.pow(td.pixFromY(b.y) + b.dy, 2)));
	}
}
