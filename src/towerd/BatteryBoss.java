package towerd;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class BatteryBoss {
	public static Image boss;
	String[] split;
	int x = 0, y = -400, xTo, yTo, xFrom, yFrom;
	boolean inPosition = false;

	static {
		try {
			boss = ImageIO.read(new File("Resources/THE_BOSS.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public BatteryBoss() {
		hideY();
	}

	public void paint(Graphics g) {
		if (x > -900 && y > -400 && x < TowerDefense.SCREEN.width && y < TowerDefense.SCREEN.height) {
			g.setColor(new Color(45, 45, 50));
			g.fillRect(x, y, 450, 400);
			g.fillRect(TowerDefense.SCREEN.width - x - 450, TowerDefense.SCREEN.height - 400 - y, 450, 400);

			g.drawImage(boss, x + 30, y + 30, 300, 300, null);

			g.setColor(Color.WHITE);
			g.setFont(new Font("SANS_SERIF", Font.PLAIN, 30));
			if (inPosition && split != null)
				for (int i = 0; i < split.length; i++)
					g.drawString(split[i], x + 350, y + 100 + 30 * i);
		}
	}

	public void show() {
		yTo = (TowerDefense.SCREEN.height - 400) / 2;
		xTo = (TowerDefense.SCREEN.width - 900) / 2;
		xFrom = x;
		yFrom = y;
	}

	public void hide() {
		if (Math.random() > .5)
			hideX();
		else
			hideY();
	}

	public void hideX() {
		yTo = (TowerDefense.SCREEN.height - 400) / 2;
		xTo = -900;
		xFrom = x;
		yFrom = y;
	}

	public void hideY() {
		yTo = -400;
		xTo = (TowerDefense.SCREEN.width - 900) / 2;
		xFrom = x;
		yFrom = y;
	}

	public void update() {
		x += Math.signum(xTo - x) * Math.ceil(Math.sqrt(Math.abs((xTo - x) * (Math.abs(x - xFrom) + 1))) / 20);
		y += Math.signum(yTo - y) * Math.ceil(Math.sqrt(Math.abs((yTo - y) * (Math.abs(y - yFrom) + 1))) / 20);

		if (Math.abs(x - xTo) < 10)
			x = xTo;
		if (Math.abs(y - yTo) < 10)
			y = yTo;

		inPosition = (x == (TowerDefense.SCREEN.width - 900) / 2 && y == (TowerDefense.SCREEN.height - 400) / 2);
	}

	public void setText(String string) {
		split = string.split("\n");
		System.out.println(split.length + "," + split[0]);
		show();
	}

}
