package towerd;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;

import util.Methods;

public class Rubbish extends GameObject {
	int number = 1;

	public Rubbish(int x, int y) {
		super(x, y);
	}

	public void update(TowerDefense td) {
	}

	public Image paint(int width, int height) {
		if (im == null || im.getWidth() != width || im.getHeight() != height) {
			// if (Math.random() > 0.95 || im == null) {
			im = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
			Graphics2D g = im.createGraphics();
			g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
					RenderingHints.VALUE_ANTIALIAS_ON);
			for (int i = 0; i < number; i++) {
				g.setColor(new Color(
						(int) (Methods.pseudoRandom(i * 5 + x + y) * 120) + 50,
						(int) (Methods.pseudoRandom(i * x * 28 + x / (y + 1)
								- y - i) * 120) + 50,
						(int) (Methods.pseudoRandom(i * i * 20 * 46 + y * x * i
								* x) * 120) + 50));

				int ix = (int) (Methods.pseudoRandom(i * 2 + x + y) * width), iy = (int) (Methods
						.pseudoRandom(i + x * y + i + y / (x + 1)) * height), w = (int) (Methods
						.pseudoRandom(800 / (i + 1) + x * i * i - y * i * i) * (width - ix)), h = (int) (Methods
						.pseudoRandom(i * 90 * x * y) * (height - iy));

				if (Methods.pseudoRandom(800 * x / (i + 1) + x * i * y * i - y
						* i * i) > .5)
					g.fillRect(ix, iy, w, h);
				else
					g.drawOval(ix, iy, w, h);

				g.setColor(Color.BLACK);
				g.fillOval(2, height - 15, 20, 13);
				g.setColor(Color.WHITE);
				g.drawString("" + number, 2, height - 3);

			}

			g.dispose();
		}

		return im;
	}

	public static void main(String args[]) {
		TowerDefense.main(args);
	}
}