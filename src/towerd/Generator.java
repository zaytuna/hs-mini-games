package towerd;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class Generator extends GameObject {
	int energyPerTurn = 50, rotation = 0;

	ArrayList<PowerLine> lines = new ArrayList<PowerLine>();

	public Generator(int x, int y) {
		super(x, y);
	}

	@Override
	public Image paint(int width, int height) {
		if (im == null || im.getWidth() != width || im.getHeight() != height) {
			im = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
			Graphics2D g = im.createGraphics();
			g.setColor(Color.ORANGE);
			g.fillRect(0, 0, width, height);
			g.setColor(Color.BLACK);
			g.drawLine(0, 0, width, height);
			g.drawLine(width, 0, 0, height);
			g.drawLine(width / 2, 0, width / 2, height);
			g.drawLine(0, height / 2, width, height / 2);
			g.fillRect(width / 3, height / 3, width / 3, height / 3);
		}
		return im;
	}

	@Override
	public void update(TowerDefense td) {
		if (!lines.isEmpty() && (rotation++ % 50) == 0)
			lines.get((rotation / 50) % lines.size()).begin(energyPerTurn);
	}
}
