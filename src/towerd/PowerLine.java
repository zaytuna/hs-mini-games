package towerd;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.ArrayList;

public class PowerLine {
	public static int numAlive = 0;

	int index = 0;
	Generator start;
	final int endX, endY;
	int radius = 1;

	ArrayList<Integer> energies = new ArrayList<Integer>();
	ArrayList<Double> prcdone = new ArrayList<Double>();

	PowerLine(Generator start, Point end) {
		this.start = start;
		start.lines.add(this);
		this.endX = end.x;
		this.endY = end.y;
		index = numAlive++;
	}

	public void paint(Graphics2D g, int x1, int y1, int x2, int y2) {
		g.setColor(Color.ORANGE);
		g.drawLine(x1, y1, x2, y2);
		for (int i = 0; i < energies.size(); i++) {
			g.setColor(Color.YELLOW);
			g.fillOval(x1 + (int) ((x2 - x1) * prcdone.get(i)) - energies.get(i) / 8, y1
					+ (int) ((y2 - y1) * prcdone.get(i)) - energies.get(i) / 8, energies.get(i) / 4,
					energies.get(i) / 4);
		}
	}

	public void update(TowerDefense td) {
		for (int i = 0; i < prcdone.size(); i++) {
			prcdone.set(i, Math.min(prcdone.get(i) + .01, 1));
			if (prcdone.get(i) == 1) {

				for (int j = -radius; j <= radius; j++)
					for (int k = -radius; k <= radius; k++)
						if (endX + j >= 0 && endY + k >= 0 && endX + j < td.map.length && endY + k < td.map[0].length) {
							td.map[endX + j][endY + k].power = Math.min(energies.get(i)
									+ td.map[endX + j][endY + k].power, 200);
							td.map[endX + j][endY + k].im = null;
						}

				prcdone.remove(i);
				energies.remove(i);
			}
		}
	}

	public void begin(int energyPerTurn) {
		prcdone.add(0D);
		energies.add(energyPerTurn);
	}
}
