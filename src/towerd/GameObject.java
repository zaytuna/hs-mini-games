package towerd;

import java.awt.Image;
import java.awt.image.BufferedImage;

public abstract class GameObject {
	public GameObject(int x, int y) {
		this.x = x;
		this.y = y;
	}

	BufferedImage im;

	public int x, y, power = 0;

	public abstract void update(TowerDefense td);

	public abstract Image paint(int width, int height);

	public static void main(String args[]) {
		TowerDefense.main(args);
	}
}