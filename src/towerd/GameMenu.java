package towerd;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;

import util.Methods;

public class GameMenu implements Runnable {
	TowerDefense td;
	boolean running = true;
	int bHeight = 0;

	double[] filled = { 0, 0, 0, 0, 0 };
	boolean[] pressed = { false, false, false, false, false };
	String[] names = { "Play Level", "High Scores", "Options", "Tutorial", "Exit" };
	Color[] colors = { Color.RED, Color.ORANGE, Color.GREEN, Color.WHITE, Color.CYAN }; 

	GameMenu(TowerDefense td) {
		this.td = td;
		new Thread(this).start();

		bHeight = (TowerDefense.SCREEN.height - 200) / names.length - 20;
	}

	public void paint(Graphics gr) {
		Graphics2D g = (Graphics2D) gr;
		g.setFont(new Font("SANS_SERIF", Font.BOLD, 30));

		for (int i = 0; i < names.length; i++)
			drawBattery(g, 100, 100 + (bHeight + 20) * i, 400, bHeight, i);
	}

	private void drawBattery(Graphics2D g, int x, int y, int w, int h, int i) {
		g.setColor(pressed[i] ? Color.DARK_GRAY : colors[i]);
		g.drawLine(x, y, x + w - 20, y);
		g.drawLine(x, y + h, x + w - 20, y + h);
		g.drawLine(x + w - 20, y, x + w - 20, y + h / 3);
		g.drawLine(x + w, y + h / 3, x + w, y + 2 * h / 3);
		g.drawLine(x + w - 20, y + 2 * h / 3, x + w - 20, y + h);
		g.drawLine(x, y, x, y + h);

		g.drawLine(x + w - 20, y + h / 3, x + w, y + h / 3);
		g.drawLine(x + w - 20, y + 2 * h / 3, x + w, y + 2 * h / 3);

		g.setColor(pressed[i] ? colors[i].darker() : colors[i]);
		g.fillRect(x + 2, y + 2, (int) Math.min(w * filled[i], w - 20) - 4, h - 4);
		g.fillRect(x + w - 23, y + h / 3 + 2, 20 - Math.min((int) (w * (1 - filled[i])), 20), h / 3 - 4);

		g.setColor(Methods.colorMeld(Color.WHITE, Color.black, filled[i]));
		Methods.drawCenteredText(g, names[i], x, y, w, h);
	}

	public void mousePressed(MouseEvent evt) {
		for (int i = 0; i < names.length; i++)
			if (new Rectangle(100, 100 + (bHeight + 20) * i, 400, bHeight).contains(evt.getPoint()))
				pressed[i] = true;
	}

	public void mouseReleased(MouseEvent evt) {
		for (int i = 0; i < names.length; i++)
			pressed[i] = false;

		if (new Rectangle(100, 100, 400, bHeight).contains(evt.getPoint())) {
			td.reset();
			td.gameMode = 0;
			new Thread(new Runnable() {
				public void run() {
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					td.boss.setText("You better get them batteries to my \ncustomers this time, boy");
				}
			}).start();
		} else if (new Rectangle(100, 100 + (bHeight + 20), 400, 200).contains(evt.getPoint())) {
		} else if (new Rectangle(100, 100 + (bHeight + 20) * 2, 400, 200).contains(evt.getPoint())) {
		} else if (new Rectangle(100, 100 + (bHeight + 20) * 3, 400, 200).contains(evt.getPoint())) {
		} else if (new Rectangle(100, 100 + (bHeight + 20) * 4, 400, 200).contains(evt.getPoint())) {
			System.exit(0);
		}
	}

	@Override
	public void run() {
		while (true) {
			do
				try {
					Thread.sleep(20);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			while (!running);

			Point mouse = Methods.mouse();

			for (int i = 0; i < names.length; i++)
				if (new Rectangle(100, 100 + (bHeight + 20) * i, 400, bHeight).contains(mouse))
					filled[i] = Math.min(1, filled[i] + .02);
				else
					filled[i] = Math.max(0, filled[i] - .01);

		}
	}
}
