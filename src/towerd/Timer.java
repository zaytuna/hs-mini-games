package towerd;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;

public class Timer extends JLabel implements Runnable {
	private static final long serialVersionUID = 106937239010198857L;
	long startTime = -1;
	long time = 0;
	boolean hasMax = false;
	int maxTimeSeconds = 0;

	private ArrayList<ActionListener> listeners = new ArrayList<ActionListener>();

	public Timer() {
		new Thread(this).start();
		setFont(new Font("SANS_SERIF", Font.BOLD, 30));
		setForeground(Color.WHITE);
		setHorizontalAlignment(JLabel.CENTER);
	}

	public void run() {
		while (true) {
			if (hasMax)
				setText(format(maxTimeSeconds * 1000 - getTime()) + "");
			else
				setText(format(getTime()) + "");

			if (startTime > 0 && hasMax && (maxTimeSeconds * 1000 <= getTime())) {
				startTime = -1;
				for (ActionListener l : listeners)
					l.actionPerformed(new ActionEvent(this,
							ActionEvent.ACTION_PERFORMED, "time is up"));
			}

			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public boolean running() {
		return startTime > 0;
	}

	public String format(long l) {
		int seconds = (int) ((l / 1000) % 60);
		int minutes = (int) ((l / 60000) % 60);
		int hours = (int) ((l / 3600000) % 24);

		String str = formatNum(seconds);
		if (minutes > 0 || hours > 0)
			str = formatNum(minutes) + ":" + str;
		if (hours > 0)
			str = hours + ":" + str;

		return str;
	}

	private String formatNum(int n) {
		if (n < 10)
			return "0" + Math.abs(n);
		return "" + Math.abs(n) % 60;
	}

	public void addActionListener(ActionListener a) {
		listeners.add(a);
	}

	public void setMax(int m) {
		hasMax = true;
		maxTimeSeconds = m;
	}

	public void start() {
		startTime = System.currentTimeMillis();
	}

	public void pause() {
		time += (System.currentTimeMillis() - startTime) / 1000;
		startTime = -1;
	}

	public void clear() {
		time = 0;
		startTime = -1;
	}

	public long getTime() {
		return time
				+ (startTime == -1 ? 0
						: (System.currentTimeMillis() - startTime));
	}

	public int getTimeSeconds() {
		return (int) (getTime() / 1000);
	}
}
