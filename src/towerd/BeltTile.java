package towerd;

import java.awt.image.*;
import java.awt.*;
import java.util.*;

public class BeltTile extends GameObject {
	public ArrayList<Point> next;
	public boolean end = false, start = false;
	private int nextCounter = 0;

	public BeltTile(int x, int y) {
		super(x, y);
		next = new ArrayList<Point>();
	}

	public void update(TowerDefense td) {
	}

	public Image paint(int width, int height) {
		if (im == null || im.getWidth() != width || im.getHeight() != height) {
			im = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
			Graphics2D g = im.createGraphics();
			g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			g.setColor(Color.cyan);
			g.fillRect(0, 0, width, height);

			g.setStroke(new BasicStroke(2));

			if (next.size() > 0) {

				for (int i = 0; i < next.size(); i++)
					if (i != nextCounter)
						drawArrow(g, i, width, height);
				drawArrow(g, nextCounter, width, height);
			}
			g.dispose();
			// im =i ;
		}

		return im;

	}

	private void drawArrow(Graphics g, int i, int width, int height) {
		int dx = (next.get(i).x - x), dy = (next.get(i).y - y);
		double theta = Math.atan2(dy, dx);
		dx = (int) (width * Math.cos(theta) * .7);
		dy = (int) (height * Math.sin(theta) * .7);
		g.setColor(nextCounter == i ? Color.BLACK : new Color(0, 195, 195));

		g.drawLine(width / 2 - dx / 2, height / 2 - dy / 2, width / 2 + dx / 2, height / 2 + dy / 2);
		g.drawLine(width / 2 + dx / 2, height / 2 + dy / 2, width / 2 + dy / 4, height / 2 - dx / 4);
		g.drawLine(width / 2 + dx / 2, height / 2 + dy / 2, width / 2 - dy / 4, height / 2 + dx / 4);
	}

	public static void main(String args[]) {
		TowerDefense.main(args);
	}

	public Point pollNext() {
		if (next.size() == 0)
			return null;
		nextCounter = (nextCounter + 1) % next.size();

		im = null;
		return next.get(nextCounter % next.size());
	}

}