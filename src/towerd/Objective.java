package towerd;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;

public class Objective extends GameObject {

	int num, seconds, secondsLeft, collected = 0;
	long startTime;

	private Objective(int x, int y) {
		super(x, y);
	}

	public static Objective create(int gridW, int gridH, int time, int n) {
		Objective o = new Objective((int) (Math.random() * gridW), (int) (Math.random() * gridH));
		o.num = n;
		o.seconds = time;
		o.secondsLeft = time;
		return o;
	}

	public static Objective createStatic(int gridW, int gridH, int n) {
		Objective o = new Objective(gridW - 1, gridH / 2);
		o.num = n;
		o.seconds = Integer.MAX_VALUE;
		o.secondsLeft = Integer.MAX_VALUE;
		return o;
	}

	public void start() {
		startTime = System.nanoTime();
	}

	@Override
	public Image paint(int width, int height) {
		if (im == null || im.getWidth() != width || im.getHeight() != height) {
			im = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
			Graphics2D g = im.createGraphics();
			g.setColor(Color.BLUE);
			g.fillRect(0, 0, width, height);
		}
		return im;
	}

	@Override
	public void update(TowerDefense td) {
		secondsLeft = (int) (seconds - (System.nanoTime() - startTime) / 1000000000);
		if (secondsLeft < 0 || collected >= num) {
			td.happiness = Math.min(td.happiness + ((collected >= num) ? 5 : -25), td.maxHappiness);

			td.map[x][y] = new BlankTile(x, y);
			td.objective = td.nextObj;
			td.objective.start();
			td.nextObj = Objective.create(td.map.length, td.map[0].length, 30, 15);
			td.map[td.objective.x][td.objective.y] = td.objective;
		}
	}

	public void collect(TowerDefense td, Battery b) {
		td.bats.remove(b);
		td.happiness = Math.min(td.happiness + ((b.filled == b.capacity) ? 1 : -5), td.maxHappiness);

		if (b.filled == b.capacity)
			collected++;

	}
}
