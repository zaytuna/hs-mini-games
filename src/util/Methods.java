package util;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Methods {
	public static void centerFrame(int frameWidth, int frameHeight, Component c) {
		Toolkit tools = Toolkit.getDefaultToolkit();
		Dimension screen = tools.getScreenSize();
		int xUpperLeftCorner = (screen.width - frameWidth) / 2;
		int yUpperLeftCorner = (screen.height - frameHeight) / 2;

		c.setBounds(xUpperLeftCorner, yUpperLeftCorner, frameWidth, frameHeight);
	}

	public static void replaceColor(BufferedImage img, Color a, Color b, int tollerence) {
		int w = img.getWidth(), h = img.getHeight();
		int[] pixels = img.getRGB(0, 0, w, h, null, 0, w);

		for (int x = 0; x < w; x++) {
			for (int y = 0; y < h; y++) {
				if (colorsAreClose(pixels[x + w * y], a.getRGB(), tollerence))
					img.setRGB(x, y, b.getRGB());
			}
		}
	}

	public static void drawCenteredText(Graphics g, String s, int x, int y, int w, int h) {
		// Find the size of string s in font f in the current Graphics context
		// g.
		FontMetrics fm = g.getFontMetrics(g.getFont());
		java.awt.geom.Rectangle2D rect = fm.getStringBounds(s, g);

		int textHeight = (int) (rect.getHeight());
		int textWidth = (int) (rect.getWidth());

		// Center text horizontally and vertically
		int p = (w - textWidth) / 2 + x;
		int q = (h - textHeight) / 2 + fm.getAscent() + y;

		g.drawString(s, p, q); // Draw the string.
	}

	public static void copy(File src, File dst) throws IOException {
		InputStream in = new FileInputStream(src);
		OutputStream out = new FileOutputStream(dst);

		// Transfer bytes from in to out
		byte[] buf = new byte[1024];
		int len;
		while ((len = in.read(buf)) > 0) {
			out.write(buf, 0, len);
		}
		in.close();
		out.close();
	}

	public static int[] ensureContains(int[] array, int... values) {
		int newLength = array.length + values.length;
		for (int i = 0; i < values.length; i++)
			if (Methods.contains(array, values[i]))
				newLength--;

		int[] newArray = new int[newLength];
		int count = 0;
		for (int v : array)
			newArray[count++] = v;

		for (int v : values)
			if (!Methods.contains(array, v))
				newArray[count++] = v;

		return newArray;
	}

	public static int[] removeAll(int[] array, int... values) {
		int newLength = array.length - values.length;
		for (int i = 0; i < values.length; i++)
			if (!Methods.contains(array, values[i]))
				newLength++;

		int[] newArray = new int[newLength];
		int count = 0;
		for (int v : array) {
			if (Methods.contains(values, v))
				continue;
			newArray[count++] = v;
		}

		return newArray;
	}

	public static <T> int indexOf(T object, T[] array) {
		for (int i = 0; i < array.length; i++)
			if (object.equals(array[i]))
				return i;
		return -1;
	}

	public static int[] ensureContainsOnly(int[] array, int... values) {
		int newLength = array.length;
		for (int i = 0; i < array.length; i++)
			if (!Methods.contains(values, array[i]))
				newLength--;

		int[] newArray = new int[newLength];
		int count = 0;
		for (int v : array) {
			if (Methods.contains(values, v))
				newArray[count++] = v;
		}

		return newArray;
	}

	public static boolean colorsAreClose(int a, int b, int t) {
		int a1 = (a >> 24) & 0xff;
		int r1 = (a >> 16) & 0xff;
		int g1 = (a >> 8) & 0xff;
		int b1 = (a >> 0) & 0xff;

		int a2 = (b >> 24) & 0xff;
		int r2 = (b >> 16) & 0xff;
		int g2 = (b >> 8) & 0xff;
		int b2 = (b >> 0) & 0xff;

		return (Math.abs(r1 - r2) <= t) && (Math.abs(g1 - g2) <= t) && (Math.abs(b1 - b2) <= t)
				&& (Math.abs(a1 - a2) <= t);
	}

	public static Color getOppositeColor(Color c) {
		return new Color(255 - c.getRed(), 255 - c.getGreen(), 255 - c.getBlue());
	}

	public static <T> void shuffle(T[] stuff) {
		for (int i = 0; i < stuff.length * 10; i++)
			swap(stuff, (int) (Math.random() * stuff.length), (int) (Math.random() * stuff.length));
	}

	public static void shuffle(int[] stuff) {
		for (int i = 0; i < stuff.length * 10; i++)
			swap(stuff, (int) (Math.random() * stuff.length), (int) (Math.random() * stuff.length));
	}

	public static <T> void swap(T[] stuff, int posA, int posB) {
		T inA = stuff[posA];
		stuff[posA] = stuff[posB];
		stuff[posB] = inA;
	}

	public static Color colorMeld(Color a, Color b, double ratio) {
		return new Color((int) (a.getRed() + (b.getRed() - a.getRed()) * ratio),
				(int) (a.getGreen() + (b.getGreen() - a.getGreen()) * ratio),
				(int) (a.getBlue() + (b.getBlue() - a.getBlue()) * ratio));
	}

	public static Point mouse() {
		return MouseInfo.getPointerInfo().getLocation();
	}

	public static void swap(int[] stuff, int posA, int posB) {
		int inA = stuff[posA];
		stuff[posA] = stuff[posB];
		stuff[posB] = inA;
	}

	public static double pseudoRandom(int x) {
		x = (x << 13) ^ x;
		return Math.abs(1.0f - ((x * (x * x * 15731 + 789221) + 1376312589) & 0x7fffffff) / 1073741824.0d);
	}

	public static Color randomColor() {
		return new Color((int) (Math.random() * 255), (int) (Math.random() * 255), (int) (Math.random() * 255), 250);
	}

	public static Color randomColor(int seed, int alpha) {
		return new Color((int) Math.abs(pseudoRandom(seed) * 255), (int) Math.abs(pseudoRandom(seed + 52) * 255),
				(int) Math.abs(pseudoRandom(seed * 5) * 255),
				alpha < 0 ? (int) Math.abs(pseudoRandom(seed * 29 + 17) * 255) : alpha);
	}

	public static Color randomColorRange(int seed, int alpha, int min, int max) {
		return new Color((int) Math.abs(pseudoRandom(seed) * (max - min) + min), (int) Math.abs(pseudoRandom(seed + 52)
				* (max - min) + min), (int) Math.abs(pseudoRandom(seed * 5) * (max - min) + min),
				alpha < 0 ? (int) Math.abs(pseudoRandom(seed * 29 + 17) * 255) : alpha);
	}

	public static <T> T[] createArray(ArrayList<T> ls, T[] ar) {
		for (int i = 0; i < ls.size(); i++)
			ar[i] = ls.get(i);
		return ar;
	}

	public static Object[] createObjectArray(ArrayList<? extends Object> ls, Object[] ar) {
		for (int i = 0; i < ls.size(); i++)
			ar[i] = ls.get(i);
		return ar;
	}

	public static <T> ArrayList<T> getList(T[] from) {
		ArrayList<T> list = new ArrayList<T>();

		for (T t : from)
			list.add(t);

		return list;
	}

	public static double max(double[][] array) {
		double max = array[0][0];
		for (double[] d : array)
			for (double number : d)
				if (number > max)
					max = number;

		return max;
	}

	public static double getMax(Collection<? extends Number> nums) {
		double max = nums.iterator().next().doubleValue();
		for (Number n : nums)
			if (n.doubleValue() > max)
				max = n.doubleValue();
		return max;

	}

	public static <T, K> int getMaxLength(Map<T, K>[] mapsz) {
		int max = 0;
		for (Map<T, K> map : mapsz)
			if (map.keySet().size() > max)
				max = map.keySet().size();
		return max;
	}

	public static double sum(double[] array) {
		double sum = 0;
		for (double d : array)
			sum += d;
		return sum;
	}

	public static int sum(List<Integer> array) {
		int sum = 0;
		for (int d : array)
			sum += d;
		return sum;
	}

	public static String getFileContents(File f) {
		try {
			StringBuilder sb = new StringBuilder();

			BufferedReader in = new BufferedReader(new FileReader(f));
			while (true) {
				String temp = in.readLine();
				if (temp == null)
					break;
				sb.append(temp + "\n");
			}
			in.close();
			return sb.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/*
	 * start at i and don't quite get to the end; thus
	 * createSubArray(0,length,Array[]) will coppy it.
	 */
	public static <T> T[] createSubArray(T[] original, int start, int end, T[] toFill) {
		if (toFill.length != end - start) {
			System.out.println("Big problem in Methods.createSubArray(T[] a, int b, int c, T[] d)");
			return null;
		}

		for (int i = start; i < end; i++) {
			toFill[i - start] = original[i];
		}
		return toFill;
	}

	public static boolean contains(int[] array, int value) {
		for (int o : array)
			if (o == value)
				return true;
		return false;
	}

	public static boolean contains(double[] array, double value) {
		for (double o : array)
			if (o == value)
				return true;
		return false;
	}

	public static <T> boolean contains(T[] array, T value) {
		for (T o : array)
			if (o.equals(value))
				return true;
		return false;
	}

	public static <T> T[] createSubArray(List<T> original, int start, int end, T[] toFill) {
		if (toFill.length != end - start) {
			System.out.println("Big problem in Methods.createSubArray(T[] a, int b, int c, T[] d)");
			return null;
		}

		for (int i = start; i < end; i++)
			toFill[i - start] = original.get(i);

		return toFill;
	}

	public static int[] unbox(Integer[] createArray) {
		int[] arr = new int[createArray.length];
		for (int i = 0; i < createArray.length; i++)
			arr[i] = createArray[i];

		return arr;
	}

	public static double[] unbox(Double[] createArray) {
		double[] arr = new double[createArray.length];
		for (int i = 0; i < createArray.length; i++)
			arr[i] = createArray[i];

		return arr;
	}

	/*
	 * http://www.dreamincode.net/code/snippet516.htm
	 */
	public static void insertionSort(double[] list) {
		int firstOutOfOrder, location;
		double temp;

		for (firstOutOfOrder = 1; firstOutOfOrder < list.length; firstOutOfOrder++) {
			// Starts at second term, goes until the end of the array.
			if (list[firstOutOfOrder] < list[firstOutOfOrder - 1]) {
				// If the two are out of order, we move the element to its
				// rightful place.
				temp = list[firstOutOfOrder];
				location = firstOutOfOrder;

				do { // Keep moving down the array until we find exactly where
						// it's supposed to go.
					list[location] = list[location - 1];
					location--;
				} while (location > 0 && list[location - 1] > temp);

				list[location] = temp;
			}
		}
	}

	public static double mean(double[] vals) {
		if (vals.length == 0)
			return 0;

		double mean = 0;

		for (double d : vals)
			mean += d;

		return mean / vals.length;
	}

	public static int mean(int[] vals) {
		if (vals.length == 0)
			return 0;

		int mean = 0;

		for (double d : vals)
			mean += d;

		return mean / vals.length;
	}

	public static double median(double[] vals) {
		if (vals.length == 0)
			return 0;
		if (vals.length == 1)
			return vals[0];

		double[] d = new double[vals.length];
		for (int i = 0; i < vals.length; i++)
			d[i] = vals[i];

		insertionSort(d);
		return d.length % 2 == 1 ? d[d.length / 2 - 1] : (d[d.length / 2 - 1] + d[d.length / 2]) / 2;
	}

	public static double mode(double[] vals) {
		HashMap<Double, Integer> occ = new HashMap<Double, Integer>();

		for (double d : vals)
			if (occ.containsKey(d))
				occ.put(d, occ.get(d) + 1);
			else
				occ.put(d, 1);

		double mode = 0;

		for (Double d : occ.keySet())
			if (occ.get(d) > (occ.get(mode) == null ? -1 : occ.get(mode)))
				mode = d;

		return mode;
	}

	public static double stdev(double[] vals) {
		if (vals.length < 2)
			return 0;

		double sum = 0;
		double mean = mean(vals);

		for (double d : vals)
			sum += (mean - d) * (mean - d);

		return Math.sqrt(sum / (vals.length - 1));
	}

	public static int[] intersection(int[] classes, int[] classes2) {
		int count = 0;
		for (int c : classes)
			if (contains(classes2, c))
				count++;

		int[] ints = new int[count];

		for (int i = 0; i < classes.length; i++)
			if (contains(classes2, classes[i]))
				ints[--count] = classes[i];

		return ints;
	}

	public static int min(int... arr) {
		int min = arr[0];
		for (int number : arr)
			if (number < min)
				min = number;
		return min;
	}

	public static int count(String text, char c) {
		int count = 0;
		for (int i = 0; i < text.length(); i++)
			if (text.charAt(i) == c)
				count++;
		return count;
	}
}