package othello;

import javax.swing.JFrame;

import util.Methods;

public class OthelloMain {
	public static void main(String[] args) {
		JFrame frame = new JFrame("Othello");
		OthelloPanel p = new OthelloPanel(8, 8);
		Methods.centerFrame(800, 800, frame);
		frame.add(p);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}
