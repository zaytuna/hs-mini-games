package othello;

import java.awt.Point;

public class OthelloAI {
	public static Point moveGreedy(OthelloPanel host, int player) {
		int w = host.board.length, h = host.board[0].length;
		int[][] values = new int[w][h];

		for (int i = 0; i < w; i++)
			for (int j = 0; j < h; j++)
				values[i][j] = OthelloPanel.getTilesTurned(i, j, player, host.board);

		Point max = new Point(0, 0);
		int maxVal = values[0][0];

		for (int j = 0; j < h; j++) {
			for (int i = 0; i < w; i++) {
				if (values[i][j] > maxVal || (values[i][j] == maxVal && Math.random() > .5)) {

					max = new Point(i, j);
					maxVal = values[i][j];
				}
			}
		}

		return max;
	}

	public static Point moveStatic(OthelloPanel host, int player) {
		int w = host.board.length, h = host.board[0].length;
		int[][] values = new int[w][h];

		for (int i = 0; i < w; i++)
			for (int j = 0; j < h; j++)
				if (OthelloPanel.computeMove(i, j, player, false, host.board)) {
					values[i][j] = ((i == 0 || i == w - 1) ? 7 : 1) * ((j == 0 || j == w - 1) ? 7 : 1);
					if (i == 1 || j == 1 || i == w - 2 || j == h - 2) {
						values[i][j] -= 40;
					}

					values[i][j] += OthelloPanel.getTilesTurned(i, j, player, host.board);
				} else
					values[i][j] = -100;

		Point max = new Point(0, 0);
		int maxVal = values[0][0];

		for (int j = 0; j < h; j++) {
			for (int i = 0; i < w; i++) {
				if (values[i][j] > maxVal) {

					max = new Point(i, j);
					maxVal = values[i][j];
				}
			}
		}

		//OthelloPanel.printBoard(values);

		return max;
	}

	public static Point move(OthelloPanel host, int player, int type) {
		if (type == 1)
			return moveGreedy(host, player);
		else if (type == 2)
			return moveStatic(host, player);

		return null;
	}
}
