package othello;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JPanel;

import util.Methods;

public class OthelloPanel extends JPanel implements MouseListener, Runnable {
	private static final long serialVersionUID = 4315765398141630352L;

	Color board1 = new Color(115, 65, 45), board2 = new Color(163, 107, 59);
	Color[] players = { Color.WHITE, Color.BLACK };
	String[] names = { "Player", "Weighted" };
	int[] scores = { 0, 0 };
	int[] ai = { 0, 0 };

	int mode = 0;// play, end

	int turn = 0;

	int[][] board;
	boolean[][] possible;

	public OthelloPanel(int x, int y) {
		setUpBoard(x, y);
		setBackground(new Color(91, 117, 94));
		addMouseListener(this);

		possible = computePossibilities(1, board);
		new Thread(this).start();
	}

	public void setUpBoard(int w, int h) {
		board = new int[w][h];
		possible = new boolean[w][h];
		for (int i = 0; i < w; i++)
			for (int j = 0; j < h; j++)
				board[i][j] = (i == w / 2 && j == w / 2 - 1 || i == w / 2 - 1 && j == w / 2) ? 1 : ((i == w / 2 - 1
						&& j == w / 2 - 1 || i == w / 2 && j == w / 2) ? 2 : 0);

		// board[0][0] = 3;
		// board[w - 1][0] = 3;
		// board[0][h - 1] = 3;
		// board[w - 1][h - 1] = 3;
	}

	public static int numPossible(boolean[][] po) {
		int p = 0;
		for (int i = 0; i < po.length; i++)
			for (int j = 0; j < po[i].length; j++)
				if (po[i][j])
					p++;

		return p;
	}

	public int[] computeScores() {
		for (int i = 0; i < players.length; i++)
			scores[i] = 0;

		for (int i = 0; i < board.length; i++)
			for (int j = 0; j < board[i].length; j++)
				if (board[i][j] != 0)
					scores[board[i][j] - 1]++;

		return scores;
	}

	public void paint(Graphics g22) {
		super.paint(g22);

		Graphics2D g = (Graphics2D) g22;

		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		int w = Math.min((getWidth() - 30) / board.length, (getHeight() - 70) / board[0].length);

		g.translate((getWidth() - w * board.length) / 2, (getHeight() - w * board[0].length) / 2 + 25);

		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board[i].length; j++) {
				g.setColor(Methods.colorMeld((i + j) % 2 == 0 ? board1 : board2, new Color(144, 156, 217),
						possible[i][j] ? .3 : 0));
				g.fillRect((int) (w * i), (int) (w * j), (int) w + 1, (int) w + 1);

				if (board[i][j] > 0) {
					g.setColor(players[board[i][j] - 1]);
					g.setStroke(new BasicStroke(w / 10));
					g.drawOval((int) (w * i + w / 6), (int) (w * j + w / 6), (int) (2 * w / 3), (int) (2 * w / 3));
					g.setStroke(new BasicStroke(1));
				}
			}
		}

		g.setColor(Color.BLACK);

		g.setStroke(new BasicStroke(2));
		g.drawRect(0, 0, w * board.length, w * board[0].length);

		g.translate(-(getWidth() - w * board.length) / 2, -(getHeight() - w * board[0].length) / 2 - 25);

		// ****** POST TRANSLATE

		if (mode == 0) {

			g.setColor(players[turn]);
			g.setFont(new Font("SANS_SERIF", Font.PLAIN, 30));
			Methods.drawCenteredText(g, names[turn] + "'s turn", 0, 0, getWidth(),
					(getHeight() - w * board[0].length) / 2 + 25);
			for (int i = 0; i < scores.length; i++) {
				g.setColor(players[i]);
				g.drawString(scores[i] + "", 3, getHeight() / 2 + (i - scores.length / 2) * 50);
			}
		}

		if (mode == 1) {
			g.setColor(new Color(0, 0, 0, 200));
			g.fillRect(150, 150, getWidth() - 300, getHeight() - 300);

			g.setFont(new Font("SANS_SERIF", Font.PLAIN, 30));

			for (int i = 0; i < scores.length; i++) {
				g.setColor(Methods.colorMeld(players[i], Color.WHITE, .5));
				g.drawString(names[i] + ": " + scores[i], 300, getHeight() / 2 + (i - scores.length / 2) * 60);
			}
		}
	}

	public Point getSquare(Point mouse) {

		int w = Math.min((getWidth() - 30) / board.length, (getHeight() - 70) / board[0].length);

		return new Point((mouse.x - (getWidth() - w * board.length) / 2) / w, (mouse.y - ((getHeight() - w
				* board[0].length) / 2 + 25))
				/ w);
	}

	public static int flipRecursively(int x, int y, int dx, int dy, int player, boolean change, int[][] bo) {

		if (x < 0 || x >= bo.length || y < 0 || y >= bo[0].length)
			return -1;
		if (bo[x][y] == 0)
			return -1;

		if (bo[x][y] == player)
			return 0;

		int result = flipRecursively(x + dx, y + dy, dx, dy, player, change, bo);
		if (result >= 0) {
			if (change)
				bo[x][y] = player;
			return result + 1;
		}

		return -1;
	}

	public static int getTilesTurned(int x, int y, int player, int[][] bo) {
		if (x < 0 || y < 0 || x >= bo.length || y >= bo[x].length || bo[x][y] != 0)
			return 0;

		int num = 0;

		// use single or so that both commands execute;
		num += Math.max(flipRecursively(x + 1, y + 1, 1, 1, player, false, bo), 0);
		num += Math.max(flipRecursively(x + 1, y - 1, 1, -1, player, false, bo), 0);
		num += Math.max(flipRecursively(x + 1, y, 1, 0, player, false, bo), 0);

		num += Math.max(flipRecursively(x, y + 1, 0, 1, player, false, bo), 0);
		num += Math.max(flipRecursively(x, y - 1, 0, -1, player, false, bo), 0);

		num += Math.max(flipRecursively(x - 1, y + 1, -1, 1, player, false, bo), 0);
		num += Math.max(flipRecursively(x - 1, y, -1, 0, player, false, bo), 0);
		num += Math.max(flipRecursively(x - 1, y - 1, -1, -1, player, false, bo), 0);

		return num;
	}

	public static boolean[][] computePossibilities(int player, int[][] bo) {
		boolean[][] poss = new boolean[bo.length][bo[0].length];
		for (int i = 0; i < bo.length; i++)
			for (int j = 0; j < bo[i].length; j++)
				poss[i][j] = computeMove(i, j, player, false, bo);
		return poss;
	}

	public boolean hasEnded(int[][] bo) {
		for (int i = 0; i < players.length; i++)
			if (numPossible(computePossibilities(i + 1, bo)) > 0)
				return false;

		return true;
	}

	public static boolean computeMove(int x, int y, int player, boolean change, int[][] bo) {
		if (x < 0 || y < 0 || x >= bo.length || y >= bo[x].length || bo[x][y] != 0)
			return false;

		boolean good = false;

		// use single or so that both commands execute;
		good = good | flipRecursively(x + 1, y + 1, 1, 1, player, change, bo) > 0;
		good = good | flipRecursively(x + 1, y - 1, 1, -1, player, change, bo) > 0;
		good = good | flipRecursively(x + 1, y, 1, 0, player, change, bo) > 0;

		good = good | flipRecursively(x, y + 1, 0, 1, player, change, bo) > 0;
		good = good | flipRecursively(x, y - 1, 0, -1, player, change, bo) > 0;

		good = good | flipRecursively(x - 1, y + 1, -1, 1, player, change, bo) > 0;
		good = good | flipRecursively(x - 1, y, -1, 0, player, change, bo) > 0;
		good = good | flipRecursively(x - 1, y - 1, -1, -1, player, change, bo) > 0;

		if (good && change)
			bo[x][y] = player;

		return good;
	}

	public static void printBoard(int[][] b) {
		for (int y = 0; y < b[0].length; y++) {
			for (int x = 0; x < b.length; x++)
				System.out.print(b[x][y] + "\t");
			System.out.println();
		}
		System.out.println();
	}

	public void run() {
		while (true) {
			try {
				 Thread.sleep(1000);
			} catch (Exception e) {
			}

			if (mode == 0) {
				if (ai[turn] > 0) {
					Point p = OthelloAI.move(this, turn + 1, ai[turn]);
					if (computeMove(p.x, p.y, turn + 1, true, board) || numPossible(possible) == 0) {
						computeScores();
						if (hasEnded(board)) {
							mode = 1;
						} else {
							do {
								turn = (turn + 1) % players.length;
								possible = computePossibilities(turn + 1, board);

							} while (numPossible(possible) == 0);
						}
					}
				}
			}
			repaint();

		}
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent evt) {
		if (mode == 0) {
			Point p = getSquare(evt.getPoint());
			if (ai[turn] == 0 && (computeMove(p.x, p.y, turn + 1, true, board) || numPossible(possible) == 0)) {
				if (hasEnded(board))
					mode = 1;
				else {
					computeScores();

					do {
						turn = (turn + 1) % players.length;
						possible = computePossibilities(turn + 1, board);
					} while (numPossible(possible) == 0);
				}
			}
		} else if (mode == 1) {
			setUpBoard(8, 8);
			mode = 0;
			turn = 0;
			possible = computePossibilities(1, board);
		}

		repaint();
	}

	public void mouseEntered(MouseEvent arg0) {
	}

	public void mouseExited(MouseEvent arg0) {
	}
}
