package evilsweeper;

import javax.swing.JFrame;

import util.Methods;

public class EvilMain {
	public static void main(String[] args) {
		JFrame frame = new JFrame("Sweeper");
		EvilPanel p = new EvilPanel(20, 34, 30);
		Methods.centerFrame(800, 800, frame);
		frame.add(p);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
