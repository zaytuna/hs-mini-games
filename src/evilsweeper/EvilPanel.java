package evilsweeper;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JPanel;

import util.Methods;

public class EvilPanel extends JPanel implements MouseListener {
	private static final long serialVersionUID = -3412172010447693247L;

	int number;

	int[][] clues;
	int[][] covered;
	Color[] colors = { Color.BLUE, new Color(0, 150, 0), new Color(150, 0, 0), new Color(150, 0, 150),
			new Color(100, 100, 0), Color.PINK, Color.ORANGE, Color.BLACK };

	int rad = 2;

	boolean show = false;

	public EvilPanel(int x, int y, int r) {
		number = r;
		setup(x, y, r);
		addMouseListener(this);
	}

	public void setup(int x, int y, int r) {
		show = false;
		System.out.print("STARTING SETUP");
		clues = new int[x][y];
		covered = new int[x][y];

		int[] nums = new int[x * y];
		for (int i = 0; i < x * y; i++)
			nums[i] = i < r ? -1 : 0;
		Methods.shuffle(nums);

		for (int i = 0; i < x; i++)
			for (int j = 0; j < y; j++) {
				clues[i][j] = nums[i * x + j];
				covered[i][j] = 1;
			}

		for (int i = 0; i < x; i++)
			for (int j = 0; j < y; j++)
				if (clues[i][j] >= 0)
					for (int k = -rad; k <= rad; k++)
						for (int l = -rad; l <= rad; l++)
							if (i + k >= 0 && i + k < clues.length && j + l >= 0 && j + l < clues[0].length) {
								clues[i][j] += clues[i + k][j + l] < 0 ? 1 : 0;
							}

		System.out.print("..DONE\n");
	}

	public void paintComponent(Graphics g22) {
		super.paintComponent(g22);

		Graphics2D g = (Graphics2D) g22;

		int w = Math.min((getWidth() - 5) / clues.length, (getHeight() - 5) / clues[0].length);

		g.translate((getWidth() - w * clues.length) / 2, (getHeight() - w * clues[0].length) / 2);
		// g.setFont(new Font("SANS_SERIF", Font.BOLD, 17));

		for (int i = 0; i < clues.length; i++)
			for (int j = 0; j < clues[i].length; j++) {

				if (covered[i][j] == 2) {
					g.setColor(new Color(150, 150, 150));
					g.fillRect(w * i, w * j, w, w);
					g.setColor(new Color(clues[i][j] > 0 && show ? 0 : 150, 0, clues[i][j] > 0 && show ? 150 : 0));
					g.fillRect(w * i + w / 6, w * j + w / 3, 2 * w / 3, w / 3);
				} else {
					if (clues[i][j] < 0) {
						g.setColor(Color.MAGENTA);
						g.fillOval(w * i, w * j, w, w);
					} else if (clues[i][j] > 0) {
						g.setColor(colors[Math.abs(clues[i][j] - 1) % colors.length]);
						Methods.drawCenteredText(g, "" + clues[i][j], w * i, w * j, w, w);
					}
				}

				if (covered[i][j] == 1) {
					g.setColor(new Color(150, 150, 150, show ? 150 : 255));
					g.fillRect(w * i, w * j, w, w);
				}

				g.setColor(Color.DARK_GRAY);
				g.drawRect(w * i, w * j, w, w);
			}

		g.translate(-(getWidth() - w * clues.length) / 2, -(getHeight() - w * clues[0].length) / 2);
	}

	public Point getSquare(Point mouse) {

		int w = Math.min((getWidth() - 5) / clues.length, (getHeight() - 5) / clues[0].length);

		return new Point((mouse.x - (getWidth() - w * clues.length) / 2) / w, (mouse.y - ((getHeight() - w
				* clues[0].length) / 2))
				/ w);
	}

	public boolean reveal(int x, int y) {
		if (x < 0 || y < 0 || x >= clues.length || y >= clues[x].length || covered[x][y] == 0)
			return true;

		if (covered[x][y] == 2) {
			covered[x][y] = 1;
			return true;
		}

		covered[x][y] = 0;

		if (clues[x][y] == 0) {
			reveal(x + 1, y + 1);
			reveal(x + 1, y);
			reveal(x + 1, y - 1);

			reveal(x, y + 1);
			reveal(x, y - 1);

			reveal(x - 1, y + 1);
			reveal(x - 1, y);
			reveal(x - 1, y - 1);
		}

		return clues[x][y] >= 0;
	}

	public boolean uncovered() {
		for (int i = 0; i < clues.length; i++)
			for (int j = 0; j < clues[i].length; j++)
				if (covered[i][j] == 1)
					return false;

		return true;
	}

	public void mouseClicked(MouseEvent arg0) {
	}

	public void mouseReleased(MouseEvent evt) {
		System.out.print("... cannot be unpressed\n");

		Point p = getSquare(evt.getPoint());

		if (!show) {

			if (evt.isMetaDown() && covered[p.x][p.y] != 2)
				covered[p.x][p.y] = 2;
			else if (!reveal(p.x, p.y))
				show = true;
		} else {
			// if (uncovered()) {
			setup(clues.length, clues[0].length, number);
			return;
			// }
		}
		repaint();
	}

	@Override
	public void mousePressed(MouseEvent evt) {
		System.out.print("What is pressed ...");
	}

	public void mouseEntered(MouseEvent arg0) {
	}

	public void mouseExited(MouseEvent arg0) {
	}
}
